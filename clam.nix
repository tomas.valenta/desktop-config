{ pkgs, ... }:

{
  users.groups.clamav = {};

  users.users.clamav = {
    isSystemUser = true;
    group = "clamav";
  };

  systemd.tmpfiles.rules = [
    "d /etc/clamav 0755 clamav clamav - -"
    "d /var/lib/clamav 0755 clamav clamav - -"
    "d /var/log/clamav 0755 clamav clamav - -"
    "d /run/clamav 0755 clamav clamav - -"
  ];

  systemd.services.clamd = {
    description = "ClamAV Daemon";
    after = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.clamav}/bin/clamd --foreground=yes";
      Restart = "on-failure";
      User = "clamav";
      Group = "clamav";
      PrivateTmp = true;
      RuntimeDirectory = "clamav";
      RuntimeDirectoryMode = "0755";
    };
  };

  systemd.services.freshclam = {
    description = "ClamAV Virus Database Updater";
    after = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.clamav}/bin/freshclam --foreground=yes";
      Restart = "on-failure";
      User = "clamav";
      Group = "clamav";
      PrivateTmp = true;
      RuntimeDirectory = "clamav";
      RuntimeDirectoryMode = "0755";
    };
  };

  environment.etc = {
    "clamav/clamd.conf".text = ''
      LogFile /var/log/clamav/clamd.log
      LogFileMaxSize 0
      LogTime true
      LogClean false
      LogSyslog false
      PidFile /run/clamav/clamd.pid
      DatabaseDirectory /var/lib/clamav
      LocalSocket /run/clamav/clamd.ctl
      FixStaleSocket true
      TCPSocket 3310
      TCPAddr 127.0.0.1
      User clamav
    '';

    "clamav/freshclam.conf".text = ''
      DatabaseDirectory /var/lib/clamav
      UpdateLogFile /var/log/clamav/freshclam.log
      LogFileMaxSize 0
      LogTime true
      LogSyslog false
      PidFile /run/clamav/freshclam.pid
      DatabaseOwner clamav
      Checks 24
      DNSDatabaseInfo current.cvd.clamav.net
      DatabaseMirror database.clamav.net
    '';
  };
}