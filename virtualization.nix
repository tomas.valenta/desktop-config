{ pkgs, ... }:

{
  # Add VirtManager
  programs.virt-manager.enable = true;

  virtualisation.containers.enable = true;
  virtualisation = {
    spiceUSBRedirection.enable = true;
    
    podman = {
      enable = true;

      # Create a `docker` alias for podman, to use it as a drop-in replacement
      dockerCompat = true;

      # Required for containers under podman-compose to be able to talk to each other.
      defaultNetwork.settings.dns_enabled = true;
    };

    # Add QEMU
    libvirtd = {
      enable = true;
      qemu = {
        package = pkgs.qemu_kvm;
        runAsRoot = true;
        swtpm.enable = true;
        ovmf = {
          enable = true;
          packages = [(pkgs.OVMF.override {
            secureBoot = true;
            tpmSupport = true;
          }).fd];
        };
      };
    };
  };

  # Disable podman-compose warning
  environment.variables = {
    PODMAN_COMPOSE_LOG_LEVEL = "ERROR";
  };
}