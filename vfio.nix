let
  # RX 580 Blackmagic eGPU
  gpuIDs = [
    "1002:67df" # Graphics
    "1002:aaf0" # Audio

    # USB bridge stuff
    "8086:15ef"
    "1022:14ea"
    "8086:15f0"
    "1022:14ef"
  ];
in { pkgs, lib, config, ... }: {
  options.vfio.enable = with lib;
    mkEnableOption "Configure the machine for VFIO";

  config = let cfg = config.vfio;
  in {
    services.udev.extraRules = ''
      # Automatically authorize the specific Thunderbolt eGPU
      ACTION=="add", SUBSYSTEM=="thunderbolt", ATTR{vendor_name}=="Blackmagic Design", ATTR{device_name}=="eGPU RX580", ATTR{authorized}="1"
    '';
    
    boot = {
      initrd.kernelModules = [
        "kvm-amd"
        "vfio"
        "vfio_pci"
        "vfio_iommu_type1"
        "kvm"
      ];

      kernelParams = [
        # enable IOMMU
        "amd_iommu=on"
        "iommu=pt"
        "vfio-pci.disable_idle_d3=1"
        "pcie_acs_override=downstream,multifunction"
      ] ++ lib.optional cfg.enable
        # isolate the GPU
        ("vfio-pci.ids=" + lib.concatStringsSep "," gpuIDs);
    };
  };
}