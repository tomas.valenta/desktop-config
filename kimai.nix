{ config, pkgs, ... }:

let
  # This kinda sucks, but I'll just keep it here until 25.05.

  nixos25_5_source = (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/e6b5e795bf244393f942987b41f48bb37879b773.tar.gz";
    sha256 = "0km07nb10l0k7rjv9d1f52is9wq7r8x59i68qznkj5z26jlhbxj2";
  });

  nixos25_5_packages = import (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/e6b5e795bf244393f942987b41f48bb37879b773.tar.gz";
    sha256 = "0km07nb10l0k7rjv9d1f52is9wq7r8x59i68qznkj5z26jlhbxj2";
  }) {
    system = "x86_64-linux";
  };
in
{
  imports = [
    "${nixos25_5_source}/nixos/modules/services/web-apps/kimai.nix"
  ];

  services.kimai = {
    sites = {
      "kimai.tld" = {
        package = nixos25_5_packages.kimai;
        database.createLocally = true;
      };
    };
  };
}