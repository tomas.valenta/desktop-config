{
  sops = {
    # This will add secrets.yml to the nix store
    # You can avoid this by adding a string to the full path instead, i.e.
    # sops.defaultSopsFile = "/root/.sops/secrets/example.yaml";
    defaultSopsFile = ./secrets/secrets.yaml;
    # Predefined age key
    age.keyFile = "/var/lib/sops-nix/key.txt";
    age.generateKey = false;
    # This is the actual specification of the secrets.
    secrets.luks = {};
    secrets.local_user_account = {};
    secrets.stringdata_vpn_password = {};
    secrets.keycloak_db_password = {};
  };
}