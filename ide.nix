{ lib, config, ... }:

{
  # Add Ollama (local AI ChatGPT-like model, used for VSCode completion)
  services.ollama.enable = true;

  # Add Redis
  services.redis.servers."cache" = {
    enable = true;
    port = 6379;
  };

  # Add PostgreSQL
  services.postgresql = {
    enable = true;
    ensureUsers = [
      # Pirate DBs
      {
        name = "majak";
        ensureDBOwnership = true;
      }
      {
        name = "nastenka";
        ensureDBOwnership = true;
      }
      {
        name = "contracts";
        ensureDBOwnership = true;
      }
      {
        name = "piroplaceni";
        ensureDBOwnership = true;
      }
      {
        name = "styleguide";
        ensureDBOwnership = true;
      }
      {
        name = "ucebnice";
        ensureDBOwnership = true;
      }
      {
        name = "odmeny";
        ensureDBOwnership = true;
      }
      {
        name = "rybicka";
        ensureDBOwnership = true;
      }
      {
        name = "octopus";
        ensureDBOwnership = true;
      }

      # Gitano DBs
      {
        name = "gitano";
        ensureDBOwnership = true;
      }

      # StringData DBs
      {
        name = "odoo";
        ensureDBOwnership = true;
      }

      # Internal-ish DBs
      {
        name = "sentry";
        ensureDBOwnership = true;
      }
    ];
    ensureDatabases = [
      # Pirate DBs
      "majak"
      "nastenka"
      "contracts"
      "piroplaceni"
      "styleguide"
      "ucebnice"
      "odmeny"
      "rybicka"
      "octopus"

      # Gitano DBs
      "gitano"

      # StringData DBs
      "odoo"

      # Internal-ish DBs
      "sentry"
    ];
    authentication = lib.mkForce ''
      # Generated file; do not edit!
      # TYPE  DATABASE        USER            ADDRESS                 METHOD
      local   all             all                                     trust
      host    all             all             127.0.0.1/32            trust
      host    all             all             ::1/128                 trust
    '';
  };

  # Add nginx (for keycloak)
  services.nginx = {
    enable = true;

    # enable recommended settings
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;

    virtualHosts = {
      "keycloak.tld" = {
        locations = {
          "/" = {
            proxyPass = "http://localhost:${toString config.services.keycloak.settings.http-port}/";
          };
        };
      };
    };
  };

  # Add keycloak
  services.keycloak = {
    enable = true;

    initialAdminPassword = "admin";

    database = {
      type = "postgresql";
      createLocally = true;

      username = "keycloak";
      passwordFile = "/run/secrets/keycloak_db_password";
    };

    settings = {
      hostname = "http://keycloak.tld";
      hostname-admin-url = "http://keycloak.tld";
      http-relative-path = "/";
      http-port = 38080;
      http-enabled = true;
    };
  };
}