{ pkgs, ... }:

{
  # Add zsh
  programs.zsh = {
    enable = true;
    enableCompletion = true;
    autosuggestions.enable = true;
    syntaxHighlighting.enable = true;
    
    ohMyZsh = {
      enable = true;
      plugins = [ "git" "thefuck" ];
      theme = "refined";
    };

    shellAliases = {
      "ns" = "nix-shell";
      "rb" = "~/.scripts/rebuild.sh";
    };
  };
}
