{
  home.file = {
    ".local/bin/volume_up.sh" = {
      text = ''
        #!/usr/bin/env zsh
        # Get the current volume of the default sink
        CURRENT_VOLUME=$(pactl get-sink-volume @DEFAULT_SINK@ | grep -o '[0-9]\+%' | head -n1 | tr -d '%')

        # Define the maximum allowed volume (150%)
        MAX_VOLUME=150

        # Define the step increment (5%)
        STEP=5

        # Calculate the new volume
        NEW_VOLUME=$((CURRENT_VOLUME + STEP))

        # Cap the volume at the maximum allowed
        if [ $NEW_VOLUME -gt $MAX_VOLUME ]; then
            NEW_VOLUME=$MAX_VOLUME
        fi

        # Set the volume
        pactl set-sink-volume @DEFAULT_SINK@ ''${NEW_VOLUME}%
      '';
      executable = true;
    };
  };
}