{ pkgs, lib, ... }:
{
  programs.kitty = {
    enable = true;
    package = pkgs.kitty;
    settings = {
      background_opacity = lib.mkForce 0.75;  # between 0.0 and 1.0
      background_blur = lib.mkForce 1;        # Set to a positive value to enable background blur
    };
  };
}