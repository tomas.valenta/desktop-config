{
  programs.git = {
    enable = true;
    userName  = "Alexa Valentová";
    userEmail = "git@imaniti.org";
    extraConfig = {
      push.autoSetupRemote = true;
      pull.rebase = false;
      core.compression = 0;
      http = {
        postBuffer = 1048576000;
        maxRequestBuffer = "100M";
      };
    };
  };
}
