{ pkgs, ... }:
{
  stylix.targets.vscode.enable = false;
  
  programs.vscode = {
    enable = true;
    package = pkgs.vscodium;
    userSettings = {
      "workbench.colorTheme" = "Catppuccin Mocha";
      "workbench.iconTheme" = "material-icon-theme";
      "workbench.productIconTheme" = "bongocat";
      "workbench.welcomePage.extraAnnouncements" = false;
      "editor.fontFamily" = "'FiraCode Nerd Font Mono'";
      "editor.fontLigatures" = true;
      "editor.tabSize" = 4;
      "extensions.autoCheckUpdates" = false;
      "extensions.autoUpdate" = false;
      "security.workspace.trust.untrustedFiles" = "open";
      "window.menuBarVisibility" = "toggle";
      "git.enableSmartCommit" = true;
      "git.confirmSync" = false;
      "git.openRepositoryInParentFolders" = "never";
      "explorer.confirmDelete" = false;
      "python.pythonPath" = "./.venv/bin/python";
      "[dart]" = {
        "editor.formatOnSave" = true;
        "editor.formatOnType" = true;
        "editor.rulers" = [
          80
        ];
        "editor.selectionHighlight" = false;
        "editor.tabCompletion" = "onlySnippets";
        "editor.wordBasedSuggestions" = "off";
      };
      "csv-edit.fontFamilyInTable" = "default";
      "docker.dockerPath" = "/run/current-system/sw/bin/docker";
      "docker.composeCommand" = "podman-compose";
      "docker.host" = "unix:///run/user/1000/podman/podman.sock";
      "dev.containers.dockerComposePath" = "podman-compose";
      "dev.containers.dockerPath" = "podman";
      "jupyter.askForKernelRestart" = false;
    };
    extensions = with pkgs.open-vsx; [
      bbenoist.nix
      ms-python.python
      ms-python.debugpy
      batisteo.vscode-django
      bradlc.vscode-tailwindcss
      pkief.material-product-icons
      privy.privy-vscode
      dart-code.flutter
      dart-code.dart-code
      catppuccin.catppuccin-vsc-icons
      catppuccin.catppuccin-vsc
      jeanp413.open-remote-ssh
      janisdd.vscode-edit-csv
      oderwat.indent-rainbow
      alefragnani.bookmarks
      aaron-bond.better-comments
      arrterian.nix-env-selector
      pixl-garden.bongocat
      tonybaloney.vscode-pets
      ritwickdey.liveserver

      # FIXME
      # (catppuccin.catppuccin-vsc.override {
      #   accent = "mauve";
      #   boldKeywords = true;
      #   italicComments = true;
      #   italicKeywords = true;
      #   extraBordersEnabled = false;
      #   workbenchMode = "default";
      #   bracketMode = "rainbow";
      #   colorOverrides = {};
      #   customUIColors = {};
      # })
    ] ++ (with pkgs.vscode-marketplace; [
      # Only in VSCode marketplace
      jcbuisson.vue
      eriklynd.json-tools
    ]) ++ (pkgs.vscode-utils.extensionsFromVscodeMarketplace [
      # Need a specific version of this
      {
        name = "jupyter-renderers";
        publisher = "ms-toolsai";
        version = "1.0.19";
        sha256 = "15333GNQZhuJGOskz0FEi3mTdGO8ocfYpfZyyUbGYbM=";
      }
      # And 1.22.2 of this for Podman
      {
        name = "vscode-docker";
        publisher = "ms-azuretools";
        version = "1.22.2";
        sha256 = "sRvd9M/gF4kh4qWxtS1xKKIvqg9hRJpRl/p/FYu2TI8=";
      }
    ]) ++ (with pkgs.vscode-extensions; [
      # And need the older version of this from the Nix repositories
      ms-toolsai.jupyter
    ]);
  };

  home.file.".config/VSCodium/User/keybindings.json".text = ''
    [
      {
          "key": "ctrl+t",
          "command": "-workbench.action.showAllSymbols"
      },
      {
          "key": "ctrl+meta+t",
          "command": "workbench.action.terminal.new"
      },
      {
          "key": "ctrl+t",
          "command": "terminal.focus"
      },
      {
          "key": "ctrl+shift+l",
          "command": "workbench.action.files.saveFiles"
      },
      {
          "key": "ctrl+shift+l",
          "command": "-selectAllSearchEditorMatches",
          "when": "inSearchEditor"
      },
      {
          "key": "ctrl+shift+l",
          "command": "-addCursorsAtSearchResults",
          "when": "fileMatchOrMatchFocus && searchViewletVisible"
      },
      {
          "key": "ctrl+shift+l",
          "command": "-editor.action.selectHighlights",
          "when": "editorFocus"
      },
      {
          "key": "shift+enter",
          "command": "-python.execSelectionInTerminal",
          "when": "editorTextFocus && !findInputFocussed && !jupyter.ownsSelection && !notebookEditorFocused && !replaceInputFocussed && editorLangId == 'python'"
      }
    ]
  '';
}
