{ config, ... }:

{
  sops = {
    age.keyFile = "/var/lib/sops-nix/key.txt"; # must have no password!
    defaultSopsFile = ./secrets/secrets.yaml;

    secrets.local_ssh_private_key = {};
    secrets.local_ssh_public_key = {};
    secrets.keepass = {};
    secrets.keyring = {};
    secrets.gpg_privkey = {};

    templates."local_ssh_private_key".content = config.sops.placeholder.local_ssh_private_key;
    templates."local_ssh_public_key".content = config.sops.placeholder.local_ssh_public_key;
  };
}