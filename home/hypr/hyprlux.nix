{inputs, ...}: {
  programs.hyprlux = {
    enable = true;

    systemd = {
      enable = true;
      target = "hyprland-session.target";
    };

    night_light = {
      enabled = true;
      # Automatic sunset and sunrise
      latitude = 50.0912;
      longitude = 14.4525;
      temperature = 3500;
    };
  };
}