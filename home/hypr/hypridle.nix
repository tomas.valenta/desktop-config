{
  home.file.".config/hypr/hypridle.conf".text = ''
    general {
      lock_cmd = pgrep hyprlock > /dev/null || hyprlock > ~/.hyprlock.log
      unlock_cmd = pkill -x "hyprlock"
      before_sleep_cmd = pgrep hyprlock > /dev/null || hyprlock > ~/.hyprlock.log
    }

    listener {
      timeout = 420
      on-timeout = systemctl suspend
    }
  '';
}