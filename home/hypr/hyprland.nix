{ pkgs, inputs, ... }:

{
  home.file.".zshrc".text = ''
    if [[ -z $DISPLAY ]] && [[ $(tty) == /dev/tty1 ]]; then
      exec Hyprland
    fi
  '';
  
  home.file.".scripts/toggle_laptop_screen.sh" = {
    text = ''
      #!${pkgs.zsh}/bin/zsh

      # Check the number of active monitors excluding eDP-1
      active_monitors=$(hyprctl monitors -j | jq '[.[] | select(.name != "eDP-1" and .disabled == false)] | length')

      # Check the current status of eDP-1
      edp_status=$(hyprctl monitors -j | jq -r '.[] | select(.name == "eDP-1") | .disabled')

      if [[ $edp_status == "false" && $active_monitors -ge 1 ]]; then
          # If eDP-1 is enabled, disable it
          hyprctl keyword monitor eDP-1,disable
      else
          # If eDP-1 is disabled, enable it with the desired resolution and position
          hyprctl keyword monitor eDP-1,2256x1504,0x0,1.333333
      fi
    '';

    executable = true;
  };

  # TODO: This could be better

  home.file.".scripts/tablet_home.sh" = {
    text = ''
      #!${pkgs.zsh}/bin/zsh

      hyprctl keyword monitor DP-3,1920x1080@60,0x1152,1
    '';

    executable = true;
  };

  home.file.".scripts/tablet_work.sh" = {
    text = ''
      #!${pkgs.zsh}/bin/zsh

      hyprctl keyword monitor DP-3,1920x1080@60,0x1080,1
    '';

    executable = true;
  };

  wayland.windowManager.hyprland = {
    enable = true;
    extraConfig = ''
      ################
      ### MONITORS ###
      ################

      # See https://wiki.hyprland.org/Configuring/Monitors/
      # Inbuilt monitor
      monitor=eDP-1,2256x1504,0x0,1.333333
      # Usual external monitors @ home
      monitor=desc:AOC Q27G4 1O0Q9HA000518,2560x1440@120,0x0,1.25
      # I have no earthly idea why this is at 2049x0. If I set it to 1920x0, the monitors overlap.
      # jksdfjksdnfjksdnfhjesnigjnwriujgmorjewgn NEVERMIND IT IS FUCKING TROJČLENKA
      # 2560 125% → 2048 100%
      # the pixels are FUCKING scaled
      monitor=desc:Microstep MSI G271 0x00001D81,1920x1080@144,2049x0,1
      # Sometimes I use this one
      monitor=desc:LG Electronics LG HD 708NTMXD2171,1366x768@60,3969x0,1
      # Drawing tablet
      monitor=desc:Wacom Tech Wacom One 12 3HH01E1000663,1920x1080@60,0x1152,1
      
      # Usual external monitors @ work
      monitor=desc:Microstep MSI MD2712P PA4HC24700557,1920x1080@100,0x0,1
      monitor=desc:LG Electronics LG HD 708NTFAD2203,1366x768@60,1920x0,1
      # TV
      monitor=desc:Philips Consumer Electronics Company Philips FTV 0x01010101,3840x2160@60,auto,2
      # Random other monitors
      monitor=,preferred,auto,1

      # unscale XWayland
      xwayland {
        force_zero_scaling = true
      }


      ###################
      ### MY PROGRAMS ###
      ###################

      # See https://wiki.hyprland.org/Configuring/Keywords/

      # Set programs that you use
      $terminal = kitty
      $fileManager = nautilus
      $browser = librewolf
      $ide = codium
      $menu = wofi --show drun
      $passManager = keepassxc
      $timeTracking = Kemai


      #################
      ### AUTOSTART ###
      #################

      # Autostart necessary processes (like notifications daemons, status bars, etc.)
      # Or execute your favorite apps at launch like this:

      # exec-once = ~/.scripts/toggle_laptop_screen.sh
      exec-once = waybar
      exec-once = hypridle
      exec-once = nm-applet
      exec-once = blueman-applet
      exec-once = nextcloud

      exec-once = wl-paste --type text --watch cliphist store # Stores only text data
      exec-once = wl-paste --type image --watch cliphist store # Stores only image data


      #############################
      ### ENVIRONMENT VARIABLES ###
      #############################

      # See https://wiki.hyprland.org/Configuring/Environment-variables/

      env = XCURSOR_SIZE,24
      env = HYPRCURSOR_SIZE,24
      
      # qt5 stylix config
      env = QT_QPA_PLATFORMTHEME,qt5ct


      #####################
      ### LOOK AND FEEL ###
      #####################

      general {
        gaps_in = 5
        gaps_out = 7
        col.active_border = 0xff9999cc 0xff9999cc 45deg
        col.inactive_border = 0xff6c7086 0xff6c7086 45deg
        border_size = 2
        layout = dwindle
      }

      misc {
        force_default_wallpaper = 0
        mouse_move_enables_dpms = true
      }

      decoration {
        rounding = 10

        blur {
          enabled = true
          xray =  true
          size = 8
          passes = 5
          new_optimizations = true
          ignore_opacity = true
        }

        shadow {
          enabled = true;
        }
      }

      windowrule = float, file_progress
      windowrule = float, confirm
      windowrule = float, dialog
      windowrule = float, download
      windowrule = float, notification
      windowrule = float, error
      windowrule = float, splash
      windowrule = float, confirmreset
      windowrule = float, title:Open File
      windowrule = float, title:Properties
      windowrule = float, title:branchdialog
      windowrule = float,nemo

      animations {
        enabled = true
        bezier=overshot,0.05,0.9,0.1,1.1
        bezier=overshot,0.13,0.99,0.29,1.
        animation=windows,1,7,overshot,slide
        animation=border,1,10,default
        animation=fade,1,10,default
        animation=workspaces,1,7,overshot,slide
      }

      dwindle {
        pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
        preserve_split = yes # you probably want this
      }

      gestures {
        workspace_swipe = yes
      }

      #############
      ### INPUT ###
      #############

      # https://wiki.hyprland.org/Configuring/Variables/#input
      input {
        kb_layout = cz
        kb_variant =
        kb_model =
        kb_options =
        kb_rules =

        follow_mouse = 1

        sensitivity = 0 # -1.0 - 1.0, 0 means no modification.

        touchpad {
          natural_scroll = false
        }
      }

      # https://wiki.hyprland.org/Configuring/Variables/#gestures
      gestures {
        workspace_swipe = false
      }

      ####################
      ### KEYBINDINGSS ###
      ####################

      # See https://wiki.hyprland.org/Configuring/Keywords/
      $mainMod = SUPER # Sets "Windows" key as main modifier

      # Basic UI binds
      bind = $mainMod, Q, exec, $terminal
      bind = $mainMod, B, exec, $browser
      bind = $mainMod, I, exec, $ide
      bind = $mainMod, O, exec, $passManager
      bind = $mainMod, W, exec, $timeTracking
      bind = $mainMod, C, killactive,
      bind = $mainMod SHIFT, Q, exec, wlogout
      bind = $mainMod, E, exec, $fileManager
      bind = $mainMod, V, togglefloating,
      bind = $mainMod, RETURN, exec, $menu
      bind = $mainMod, P, exec, cliphist list | wofi --dmenu | cliphist decode | wl-copy
      bind = $mainMod, J, togglesplit, # dwindle
      bind = $mainMod, F, fullscreen

      # Move focus with mainMod + arrow keys
      bind = $mainMod, left, movefocus, l
      bind = $mainMod, right, movefocus, r
      bind = $mainMod, up, movefocus, u
      bind = $mainMod, down, movefocus, d

      # Switch workspaces with mainMod + [0-9]
      # Note - 0-9 keys have been replaced with the characters
      # the Czech keyboard layout emits on these keys.
      bind = $mainMod, code:10, workspace, 1
      bind = $mainMod, code:11, workspace, 2
      bind = $mainMod, code:12, workspace, 3
      bind = $mainMod, code:13, workspace, 4
      bind = $mainMod, code:14, workspace, 5
      bind = $mainMod, code:15, workspace, 6
      bind = $mainMod, code:16, workspace, 7
      bind = $mainMod, code:17, workspace, 8
      bind = $mainMod, code:18, workspace, 9
      bind = $mainMod, code:19, workspace, 10

      # Move active window to a workspace with mainMod + SHIFT + [0-9]
      # Note - 0-9 keys have been replaced with the characters
      # the Czech keyboard layout emits on these keys.
      bind = $mainMod SHIFT, code:10, movetoworkspace, 1
      bind = $mainMod SHIFT, code:11, movetoworkspace, 2
      bind = $mainMod SHIFT, code:12, movetoworkspace, 3
      bind = $mainMod SHIFT, code:13, movetoworkspace, 4
      bind = $mainMod SHIFT, code:14, movetoworkspace, 5
      bind = $mainMod SHIFT, code:15, movetoworkspace, 6
      bind = $mainMod SHIFT, code:16, movetoworkspace, 7
      bind = $mainMod SHIFT, code:17, movetoworkspace, 8
      bind = $mainMod SHIFT, code:18, movetoworkspace, 9
      bind = $mainMod SHIFT, code:19, movetoworkspace, 10

      # Scroll through existing workspaces.
      # mod + shift + up/down = builtin monitor
      # mod + PgUp/PgDown = secondary monitor

      bind = $mainMod SHIFT, left, workspace, r-1
      bind = $mainMod SHIFT, right, workspace, r+1

      # Move/resize windows with mainMod + LMB/RMB and dragging
      bindm = $mainMod, mouse:272, movewindow
      bindm = $mainMod, mouse:273, resizewindow

      bind = $mainMod, P, exec, hyprctl dispatch pin

      # Special workspace (scratchpad)
      bind = $mainMod, S, togglespecialworkspace, magic
      bind = $mainMod SHIFT, S, movetoworkspace, special:magic

      # Screenshots
      bind = $mainMod, code:107, exec, hyprshot -m region --clipboard-only

      # Power off
      bind = , xf86poweroff, exec, wlogout

      # Lock
      bind = $mainMod SHIFT, L, exec, hyprlock

      # Sink volume toggle mute
      bind = , XF86AudioMute, exec, swayosd-client --output-volume mute-toggle
      # Source volume toggle mute
      bind = , XF86AudioMicMute, exec, swayosd-client --input-volume mute-toggle

      # Sink volume raise with custom value
      bind = , XF86AudioRaiseVolume, exec, swayosd-client --output-volume +5 --max-volume 120
      # Sink volume lower with custom value
      bind = , XF86AudioLowerVolume, exec, swayosd-client --output-volume -5

      # Capslock (If you don't want to use the backend)
      bind = , Caps_Lock, exec, swayosd-client --caps-lock

      # Num lock
      bind = , Num_Lock, exec, swayosd-client --num-lock

      # Brightness raise with custom value('+' sign needed)
      bind = , XF86MonBrightnessUp, exec, swayosd-client --brightness +5
      # Brightness lower with custom value('-' sign needed)
      bind = , XF86MonBrightnessDown, exec, swayosd-client --brightness -5

      # Media controls
      bind = , XF86AudioPlay, exec, playerctl play-pause
      bind = , XF86AudioNext, exec, playerctl next
      bind = , XF86AudioPrev, exec, playerctl previous

      # When the usual home external monitors are plugged in, I want to turn off the laptop screen.
      bind = $mainMod SHIFT, M, exec, ~/.scripts/toggle_laptop_screen.sh

      bind = $mainMod, Home, exec, ~/.scripts/tablet_home.sh
      bind = $mainMod, End, exec, ~/.scripts/tablet_work.sh

      ##############################
      ### WINDOWS AND WORKSPACES ###
      ##############################

      plugin {
        # split-monitor-workspaces {
        #   count = 10
        #   keep_focused = 0
        #   enable_notifications = 0
        #   enable_persistent_workspaces = 1
        # }
        hyprexpo {
          columns = 3
          gap_size = 5
          bg_col = rgb(111111)
          workspace_method = center current # [center/first] [workspace] e.g. first 1 or center m+1

          enable_gesture = true # laptop touchpad
          gesture_fingers = 3  # 3 or 4
          gesture_distance = 300 # how far is the "max"
          gesture_positive = true # positive = swipe down. Negative = swipe up.
        }
      }

      # See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
      # See https://wiki.hyprland.org/Configuring/Workspace-Rules/ for workspace rules

      windowrule = float, class:^(pavucontrol)$
      windowrule = float, class:^(nm-connection-editor)$
      windowrule = float, class:^(librewolf)$,title:^(Picture-in-Picture)$

      windowrule = float, title:^(Kemai - Kimai client)$
      windowrule = size 229 473, title:^(Kemai - Kimai client)$

      windowrulev2 = suppressevent maximize, class:.* # You'll probably like this.
    '';
  };
}
