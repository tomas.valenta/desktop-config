{ pkgs, ... }:
{
  services.hyprpaper = {
    enable = true;
    package = pkgs.hyprpaper;
    # Wallpaper set using stylix
    
    settings = {
      splash = true;
      splash_offset = 6;
    };
  };
}