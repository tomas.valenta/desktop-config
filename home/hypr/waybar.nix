{ pkgs, ... }:
{
  programs.waybar = {
    enable = true;
    package = pkgs.waybar;
    settings = {
      mainBar = {
        layer = "bottom";
        position = "bottom";
        mod = "dock";
        exclusive = true;
        passthrough = false;
        height = 36;
        modules-left = [
          "hyprland/workspaces"
        ];
        modules-center = [];
        modules-right = [
          "tray"
          "pulseaudio"
          "cpu"
          "memory"
          "temperature"
          "battery"
          "backlight"
          "clock"
        ];
        "hyprland/workspaces" = {
          "icon-size" = 32;
          spacing = 16;
          "on-scroll-up" = "hyprctl dispatch workspace r+1";
          "on-scroll-down" = "hyprctl dispatch workspace r-1";
        };
        tray = {
          "icon-size" = 15;
          spacing = 6;
        };
        cpu = {
          "format" = "    {usage}%";
          "interval" = 2;
          "states" = {
            "critical" = 90;
          };
        };
        temperature = {
          "critical-threshold" = 85;
          format = " {icon}  {temperatureC}°C";
          "hwmon-path" = "/sys/class/thermal/thermal_zone3/temp";
          "format-icons" = [
            ""
            ""
            ""
            ""
          ];
        };
        memory = {
          format = "     {percentage}%";
          interval = 2;
          states = {
            critical = 100;
          };
        };
        clock = {
          format = " {:%R - %m/%d} ";
          "tooltip-format" = "<tt><small>{calendar}</small></tt>";
          calendar = {
            mode = "month";
            "mode-mon-col" = 3;
            "weeks-pos" = "right";
            "on-scroll" = 1;
            "on-click-right" = "mode";
            format = {
              months = "<span color='#ffead3'><b>{}</b></span>";
              days = "<span color='#ecc6d9'><b>{}</b></span>";
              weeks = "<span color='#99ffdd'><b>W{}</b></span>";
              weekdays = "<span color='#ffcc66'><b>{}</b></span>";
              today = "<span color='#ff6699'><b><u>{}</u></b></span>";
            };
          };
          actions = {
            "on-click-right" = "mode";
            "on-click-forward" = "tz_up";
            "on-click-backward" = "tz_down";
            "on-scroll-up" = "shift_up";
            "on-scroll-down" = "shift_down";
          };
        };
        battery = {
          states = {
            good = 95;
            warning = 30;
            critical = 20;
          };
          format = "{icon} {capacity}%";
          "format-charging" = "   {capacity}% ";
          "format-plugged" = "   {capacity}% ";
          "format-alt" = " {time} {icon} ";
          "format-icons" = [
            "󰂎"
            "󰁺"
            "󰁻"
            "󰁼"
            "󰁽"
            "󰁾"
            "󰁿"
            "󰂀"
            "󰂁"
            "󰂂"
            "󰁹"
          ];
        };
        backlight = {
          device = "intel_backlight";
          format = " {icon}  {percent}% ";
          format-icons = ["" "" "" "" "" "" "" "" ""];
          tooltip = true;
          tooltip-format = "Brightness {percent}%";
        };
        pulseaudio = {
          "max-volume" = 100;
          format = " {icon}    {volume}% ";
          "tooltip-format" = "Volume {volume}% ";
          "format-muted" = "";
          "format-icons" = {
            default = [
              ""
              ""
              ""
            ];
          };
          "on-click" = "pavucontrol";
        };
      };
    };
    style = ''
      /*base background color*/
      @define-color bg_main #181825;
      @define-color bg_main_tooltip rgba(0, 0, 0, 0.7);


      /*base background color of selections */
      @define-color bg_hover rgba(200, 200, 200, 0.3);
      /*base background color of active elements */
      @define-color bg_active rgba(100, 100, 100, 0.5);

      /*base border color*/
      @define-color border_main rgba(255, 255, 255, 0.2);

      /*text color for entries, views and content in general */
      @define-color content_main white;
      /*text color for entries that are unselected */
      @define-color content_inactive rgba(255, 255, 255, 0.25);

      * {
        text-shadow: none;
        box-shadow: none;
        border: none;
        border-radius: 0;
        font-family: "Inter";
        font-weight: 600;
        font-size: 12.7px;
      }

      window#waybar {
        background:  @bg_main;
        color: @content_main;
      }

      tooltip {
        background: @bg_main_tooltip;
        border-radius: 5px;
        border-width: 1px;
        border-style: solid;
        border-color: @border_main;
      }
      tooltip label{
        color: @content_main;
      }

      #workspaces {
        color: transparent;
        margin-right: 1.5px;
        margin-left: 1.5px;
      }
      #workspaces button {
        padding: 3px;
        color: @content_inactive;
        transition: all 0.25s cubic-bezier(0.165, 0.84, 0.44, 1);
      }
      #workspaces button.active {
        color: @content_main;
        border-bottom: 3px solid white;
      }
      #workspaces button.focused {
        color: @bg_active;
      }
      #workspaces button.urgent {
        background:  rgba(255, 200, 0, 0.35);
        border-bottom: 3px solid @warning_color;
        color: @warning_color;
      }
      #workspaces button:hover {
        background: @bg_hover;
        color: @content_main;
      }

      #window {
        border-radius: 10px;
        margin-left: 20px;
        margin-right: 20px;
      }

      #tray{
        margin-left: 5px;
        margin-right: 5px;
      }
      #tray > .passive {
        border-bottom: none;
      }
      #tray > .needs-attention {
        border-bottom: 3px solid @warning_color;
      }
      #tray > widget {
        transition: all 0.25s cubic-bezier(0.165, 0.84, 0.44, 1);
      }

      #pulseaudio {
        font-family: "JetBrainsMono Nerd Font";
        padding-left: 3px;
        padding-right: 3px;
        transition: all 0.25s cubic-bezier(0.165, 0.84, 0.44, 1);
      }

      #network {
        padding-left: 3px;
        padding-right: 3px;
      }

      #language {
        padding-left: 5px;
        padding-right: 5px;
      }

      #clock {
        padding-right: 5px;
        padding-left: 5px;
        transition: all 0.25s cubic-bezier(0.165, 0.84, 0.44, 1);
      }
    '';
  };
}