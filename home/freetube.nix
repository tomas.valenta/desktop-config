{
    programs.freetube = {
        enable = true;
        settings = {
            baseTheme = "catppuccinMocha";
            defaultQuality = "1080";
            checkForUpdates = false;
            checkForBlogPosts = false;
            hideActiveSubscriptions = false;
            hidePopularVideos = false;
            hideTrendingVideos = true;
            hideRecommendedVideos = true;
            hideSubscriptionsShorts = false;
            hideSubscriptionsVideos = false;
            mainColor = "CatppuccinMochaMauve";
            playNextVideo = true;
            rememberHistory = true;
            saveWatchedProgress = true;
            secColor = "CatppuccinMochaLavender";
            useDeArrowThumbnails = true;
            useDeArrowTitles = true;
            useSponsorBlock = true;
            defaultTheatreMode = true;
            listType = "grid";
        };
    };
}