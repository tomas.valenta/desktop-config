{
  programs.librewolf = {
    enable = true;
    settings = {
      "webgl.disabled" = false;
      "signon.rememberSignons" = false;
      "security.OCSP.require" = true;
      "privacy.resistFingerprinting" = true;
      "privacy.resistFingerprinting.autoDeclineNoUserInputCanvasPrompts" = true;
      "privacy.fingerprintingProtection" = true;
      "network.trr.mode" = 3;
      "network.trr.uri" = "https://dns11.quad9.net/dns-query";
      "network.trr.excluded-domains" = "intranet.pirati.cz";
      "network.trr.custom_uri" = "https://dns11.quad9.net/dns-query";
      "dom.security.https_only_mode" = true;
      "dom.security.https_only_mode_ever_enabled" = true;
      "browser.urlbar.suggest.searches" = false;
      "browser.urlbar.showSearchSuggestionsFirst" = false;
      "devtools.selfxss.count" = 0;  # console output breaks otherwise for some reason.
      # https://codeberg.org/librewolf/issues/issues/404
    };
  };

  stylix.targets.librewolf.enable = true;
}