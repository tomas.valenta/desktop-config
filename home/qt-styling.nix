{ pkgs, config, ... }: {
    home.file = {
        ".config/qt5ct/colors/oomox-current.conf".source = config.lib.stylix.colors {
                template = builtins.readFile ./qt-configs/oomox-current.conf.mustache;
                extension = ".conf";
            };
        ".config/Trolltech.conf".source = config.lib.stylix.colors {
                template = builtins.readFile ./qt-configs/trolltech.conf.mustache;
                extension = ".conf";
            };
        ".config/kdeglobals".source = config.lib.stylix.colors {
                template = builtins.readFile ./qt-configs/trolltech.conf.mustache;
                extension = "";
            };
        ".config/qt5ct/qt5ct.conf".text = pkgs.lib.mkBefore (builtins.readFile ./qt-configs/qt5ct.conf);
    };

    home.packages = with pkgs; [
        libsForQt5.qt5ct
        libsForQt5.breeze-qt5
    ];
    
    qt = {
        enable = true;
        style.package = pkgs.libsForQt5.breeze-qt5;
        style.name = "breeze-dark";
    };
}