{ pkgs, ... }:

{
  home.file.".scripts/pg_up.sh" = {
    text = ''
      #!${pkgs.zsh}/bin/zsh

      systemctl start postgresql

      echo "Started PostgreSQL"
    '';

    executable = true;
  };

  home.file.".scripts/pg_dn.sh" = {
    text = ''
      #!${pkgs.zsh}/bin/zsh

      systemctl stop postgresql

      echo "Stopped PostgreSQL"
    '';

    executable = true;
  };

  # Hacky Gitano DB mounting
  home.file.".scripts/pg_perms.sh" = {
    text = ''
      #!${pkgs.zsh}/bin/zsh

      chown -R postgres:postgres /run/media/alexa/Please_Live
      chown postgres:postgres /run/media/alexa

      echo "Changed permissions successfully"

      systemctl restart postgresql

      echo "Restarted PostgreSQL"
    '';

    executable = true;
  };

  # https://www.reddit.com/r/NixOS/comments/10107km/how_to_delete_old_generations_on_nixos/
  home.file.".scripts/clean_nix_generations.sh" = {
    text = ''
      #!${pkgs.zsh}/bin/zsh

      nix-collect-garbage --delete-old
      doas nix-collect-garbage -d
      doas /run/current-system/bin/switch-to-configuration boot
    '';
    executable = true;
  };

  # StringData VPN up
  home.file.".scripts/sd_up.sh" = {
    text = ''
      #!${pkgs.zsh}/bin/zsh

      ipsec up stringdata
    '';

    executable = true;
  };

  # StringData VPN down
  home.file.".scripts/sd_dn.sh" = {
    text = ''
      #!${pkgs.zsh}/bin/zsh

      ipsec down stringdata
    '';

    executable = true;
  };

  # Rebuild NixOS
  home.file.".scripts/rebuild.sh" = {
    text = ''
      #!${pkgs.zsh}/bin/zsh

      cd ~/Projects/desktop-config
      ${pkgs.git}/bin/git add *
      ${pkgs.git}/bin/git commit --message "[Automatic update on rebuild]"
      ${pkgs.git}/bin/git push

      doas rm -fr /etc/nixos/*
      doas cp -r ~/Projects/desktop-config/* /etc/nixos/
      doas nixos-rebuild switch --flake /etc/nixos#fw
    '';

    executable = true;
  };
}