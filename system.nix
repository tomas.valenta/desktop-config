{ lib, pkgs, ... }:

{
  nix = {
    package = pkgs.nixVersions.stable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    settings.auto-optimise-store = true;
  };

  # Framework-specific power management
  powerManagement = {
    enable = true;
    powertop.enable = true;
    cpuFreqGovernor = lib.mkDefault "ondemand";
  };

  # Enable thermal data
  services.thermald.enable = true;

  # Enable CUPS to print documents
  services.printing.enable = true;

  # Enable GPG
  services.pcscd.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # Enable fingerprint sensor
  services.fprintd.enable = true;

  # Enable power management
  services.upower.enable = true;

  # Sleep on power button press
  services.logind.extraConfig = ''
    HandlePowerKey=ignore
    HandlePowerKeyLongPress=poweroff
    PowerKeyIgnoreInhibited=yes
  '';

  # Enable OpenGL
  hardware.graphics.enable = true;

  # Use zsh in nix-shell
  environment.variables = {
    NIX_SHELL_ALIAS = "nix-shell --run $SHELL";
  };

  system.stateVersion = "23.11";
}