{
  networking.hostName = "fw"; # Define your hostname.

  # Add extra keycloak host
  networking.extraHosts =
  ''
    127.0.0.1 keycloak.tld
    127.0.0.1 kimai.tld
  '';

  # Enable networking

  networking.networkmanager = {
    enable = true;
    enableStrongSwan = true;
  };

  # StrongSwan work VPN

  services.strongswan = {
    enable = true;

    ca = {
      lets_encrypt = {
        auto = "add";
        cacert = "/etc/nixos/certs/isrgrootx1.pem";
      };
    };

    connections = {
      stringdata = {
        auto = "add";
        
        right = "gate.stringdata.cz";       # VPN server address
        rightid = "gate.stringdata.cz";
        rightsubnet = "10.0.0.0/8";         # Only use the VPN for internal resources
        rightauth = "pubkey";

        leftauth = "eap";
        leftsourceip = "%config";
        eap_identity = "valentua";          # Maps to user=valentua

        fragmentation = "yes";              # Encapsulation is enabled
        compress = "yes";                   # Maps to ipcomp=yes
      };
    };

    secrets = [
      "/run/secrets/stringdata_vpn_password"
    ];
  };

  # Enable bluetooth
  hardware.bluetooth.enable = true;

  networking.firewall = { 
    enable = true;
    allowedTCPPorts = [
      80 443  # HTTP
      27040   # Steam local file transfer
    ];
    allowedTCPPortRanges = [ 
      { from = 1714; to = 1764; }  # KDE Connect
    ];  
    allowedUDPPortRanges = [ 
      { from = 1714; to = 1764; }    # KDE Connect
      { from = 27031; to = 27036; }  # Steam local file transfer
    ];
  };

  # Route all DNS requests through Tor
  # services = {
  #   tor = {
  #     enable = true;
  #     client = {
  #       enable = true;
  #       dns.enable = true;
  #     };
  #     settings.DNSPort = [{
  #       addr = "127.0.0.1";
  #       port = 53;
  #     }];
  #   };
  #   resolved = {
  #     enable = true; # For caching DNS requests.
  #     fallbackDns = [ "" ]; # Overwrite compiled-in fallback DNS servers.
  #   };
  #   privoxy = {
  #     enable = true;
  #     settings = {
  #       "forward-socks5t" = "/ 127.0.0.1:9050 .";
  #     };
  #   };
  # };

  networking.nameservers = [ "9.9.9.9" ];
}