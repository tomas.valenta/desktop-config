{ pkgs, swww, ... }:

{
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Enable udisks2, devmon and GVFS (primarily for Gnome applications which rely on it)
  # GVFS is required for opening trash://
  services.udisks2.enable = true;
  services.devmon.enable = true;
  services.gvfs.enable = true;

  # Accept Android license (eww)
  nixpkgs.config.android_sdk.accept_license = true;

  # Enable ADB  
  programs.adb.enable = true;

  # Enable flatpaks
  services.flatpak.enable = true;
  xdg.portal.enable = true;  # Required for flatpaks, not sure what this does honestly
  xdg.portal.config.common.default = "*";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    curl
    wget
    nh
    gnumake
    fprintd  # Fingerprint authentication
    gnupg  # GPG stuff
    doas-sudo-shim  # sudo → doas
    openssl  # OpenSSL lib (required in some projects)
    strongswan  # Strongswan VPN
    wireguard-tools  # WireGuard VPN
    pre-commit  # Running e.g. black before committing
    lm_sensors  # System temp. sensors and such
    cifs-utils
    samba  # Shared network filesystems
    clamav  # Clam antivirus (required in some dev projects)
    jmtpfs
    unzip  # Unzip files in CLI
    exfatprogs  # ExFAT filesystem stuff
    ntfs3g  # NTFS filesystem stuff
    file  # Primarily for libmagic in some projects
    usbutils  # `lsusb`, etc.
    psmisc  # Primarily for `fuser`
    jq  # JSON Processor I use in some scripts
    spice-gtk  # Printing
    age  # Encryption software
    ssh-to-age  # Convert SSH keys to AGE keys
    dive  # look into docker image layers
    podman-tui  # status of containers in the terminal
    podman-compose  # start group of containers for dev
    opentabletdriver  # graphics tablet driver
    micro  # Nice text editor

    thefuck  # Command correction

    # Development stuff
    python311Full
    nodePackages.nodejs
    android-studio

    # Useful libraries
    gcc
    libgcc
    gnumake
    cmake
    extra-cmake-modules

    # Theming
    papirus-icon-theme
    catppuccin-sddm
    noto-fonts-cjk-sans
    noto-fonts-cjk-serif

    # Gnome pkgs
    gnome-keyring
    seahorse
  ];
}
