{ pkgs, ... }:

{
  # Bootloader config
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Add NTFS support
  boot.supportedFilesystems = [ "ntfs" ];

  boot.kernelParams = [
    # Framework-specific sleep option
    "mem_sleep_default=deep"
    # An attempt to fix Logitech devices disconnecting on reboot
    "usbcore.autosuspend=-1"
    # Increase AMD eGPU link speed
    "amdgpu.pcie_gen_cap=0x40000"
  ];

  # Use older kernel, as 6.12 causes terrible graphical and input glitching.
  boot.kernelPackages = pkgs.linuxPackages_6_11;

  boot.initrd.luks.devices."luks-bd5258a4-76c9-46a7-a8b6-118b1ebf0f22".device = "/dev/disk/by-uuid/bd5258a4-76c9-46a7-a8b6-118b1ebf0f22";
}
