Run in shell

```bash
git clone https://github.com/openvenues/libpostal
cd libpostal

nix-shell -p autoconf pkg-config automake libtool

./bootstrap.sh
./configure

# Need `doas` to output to /usr/local. Not a Nix way of
# doing this, but whatever, it works.
doas make -j8

doas make install

# This doesn't work on NixOS and idk what it does
# either so it can probably fuck off
# doas ldconfig