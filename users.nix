{ pkgs, config, ... }:

{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.alexa = {
    isNormalUser = true;
    description = "Alexa";
    extraGroups = [ "networkmanager" "libvirtd" "docker" "dialout" "adbusers" "kvm" "keys" "disk" "wheel" ];
    hashedPasswordFile = config.sops.secrets.local_user_account.path;
    shell = pkgs.zsh;

    packages = with pkgs; [
      # Basic desktop programs
      webcamoid
      keepassxc
      gimp
      drawio
      inkscape
      mpv
      super-slicer-latest
      libreoffice
      quickemu
      pavucontrol
      rpi-imager
      raider
      obs-studio
      woeusb
      flatpak  # Sandboxed program package manager
      lnav  # Logfile navigator
      rig  # Random identity generator. Idk, might be useful.
      btop  # Nice system monitor
      wev  # Useful for debugging keybinds
      kemai  # Kimai desktop GUI
      pg_activity  # PostgreSQL activity viewer
      poetry  # Run tools and stuff in Python projects
      poedit  # .po translation file editor
      rnote  # Drawing tablet notes
      krita  # Fancier drawing
      cava  # Music visualizer
      cavalier  # Music visualizer 2

      # Logitech receiver stuff
      solaar
      logitech-udev-rules

      # Gnome pkgs
      file-roller
      nautilus
      gnome-disk-utility
      gnome-calculator
      gnome-system-monitor
      gnome-calendar
      gnome-clocks
      eog
      gnome-font-viewer
      gnome-power-manager
      baobab  # Disk usage analyzer

      # Window manager
      hyprpicker
      hypridle
      hyprlock
      hyprshot
      hyprshade
      cliphist
      wl-clipboard
      brightnessctl
      wlogout

      # Network connectivity
      system-config-printer
      networkmanagerapplet
      blueman
      nextcloud-client
      nmap
      filezilla
      jellyfin-media-player
      thunderbird
      josm
      ungoogled-chromium
      remmina
      dbeaver-bin
      qbittorrent
      kdePackages.kdeconnect-kde
      signal-desktop
      motrix  # Download manager
      wirelesstools  # WiFi channel analytics etc.
      linphone  # Primarily for work bullshit

      # Games
      osu-lazer
      the-powder-toy
    ];
  };
}
