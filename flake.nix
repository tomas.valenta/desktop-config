{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    stylix.url = "github:danth/stylix/release-24.11";
    flake-utils.url = "github:numtide/flake-utils";
    # FIXME
    catppuccin-vsc.url = "https://flakehub.com/f/catppuccin/vscode/*.tar.gz";

    nix-vscode-extensions = {
      url = "github:nix-community/nix-vscode-extensions";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    hyprlux.url = "github:amadejkastelic/Hyprlux";
    sops-nix.url = "github:Mic92/sops-nix";
  };

  outputs = {
    self, nixpkgs, home-manager, stylix, flake-utils, catppuccin-vsc,
    nix-vscode-extensions, hyprlux, sops-nix, ...
  } @ inputs: let
    inherit (self) outputs;
    lib = nixpkgs.lib;

    # Function: importDir
    #
    # Dynamically imports Nix expressions from `.nix` files in a specified directory
    # and merges them into a single attribute set (`attrset`).
    #
    # Parameters:
    # - dir: A string representing the path to the directory containing `.nix` files.
    #
    # Returns:
    # A merged attribute set containing the imported Nix expressions.
    #
    importDir = dir: 
      let
        # Step 1: Filter `.nix` files
        files = builtins.filter (file: lib.strings.hasSuffix ".nix" file) (builtins.attrNames (builtins.readDir dir));
        
        # Step 2: Import `.nix` files
        modules = map (file: import (lib.strings.concatStrings [ dir  "/"  file ])) files;
      in
        # Step 3: Merge imported modules into a single attribute set
        lib.mkMerge modules;

  in {
    nixosConfigurations.fw = lib.nixosSystem {
      modules = [
        ./hardware-configuration.nix
        ./boot.nix
        ./networking.nix
        ./locales.nix
        ./sound.nix
        ./graphics.nix
        ./security.nix
        ./clam.nix
        ./users.nix
        ./flatpak.nix
        ./packages.nix
        ./virtualization.nix
        ./shell.nix
        ./ide.nix
        ./kimai.nix
        ./system.nix
        ./printing.nix
        ./vfio.nix
        ./secrets.nix

        stylix.nixosModules.stylix
        sops-nix.nixosModules.sops

        {
          nixpkgs.overlays = [
            nix-vscode-extensions.overlays.default
            catppuccin-vsc.overlays.default
          ]; 
        }

        home-manager.nixosModules.home-manager
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;

          home-manager.sharedModules = [
            sops-nix.homeManagerModules.sops
          ];

          home-manager.extraSpecialArgs = {inherit inputs outputs;};

          home-manager.users.alexa = lib.mkMerge [
            (importDir ./home)
            (importDir ./home/hypr)

            hyprlux.homeManagerModules.default
          ];
        }

        {
          specialisation."VFIO".configuration = {
            system.nixos.tags = [ "with-vfio" ];
            vfio.enable = true;
          };
        }
      ];
    };
  };
}
